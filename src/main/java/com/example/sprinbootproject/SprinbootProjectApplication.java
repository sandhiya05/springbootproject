package com.example.sprinbootproject;

import com.example.sprinbootproject.model.Employee;
import com.example.sprinbootproject.repository.EmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprinbootProjectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(SprinbootProjectApplication.class, args);
	}
	@Autowired
	private EmployeeRepository employeeRepository;

	@Override
	public void run(String... args) throws Exception {
		Employee employee = new Employee();
		employee.setFirstName("Sandhiya");
		employee.setLastName("V");
		employee.setEmailId("sandhiyagct@gmail.com");
		employeeRepository.save(employee);

		Employee employee1 = new Employee();
		employee1.setFirstName("Kaushek");
		employee1.setLastName("Rahul");
		employee1.setEmailId("kaushek11@gmail.com");
		employeeRepository.save(employee1);
	}
}
